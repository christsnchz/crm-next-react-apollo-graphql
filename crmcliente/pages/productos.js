import React from 'react';
import Layout from '../components/layout';

const Productos = () => {
  return (  
    <>
      <Layout>
        <h1 className="text-2xl text-gray-800 font-light" > Productos </h1>
      </Layout>
    </>
  );
}
 
export default Productos;