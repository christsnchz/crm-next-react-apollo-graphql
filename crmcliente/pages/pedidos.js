import React from 'react';
import Layout from '../components/layout';

const Pedidos = () => {
  return (  
    <>
      <Layout>
        <h1 className="text-2xl text-gray-800 font-light" > Pedidos </h1>
      </Layout>
    </>
  );
}
 
export default Pedidos;